Vue.component('chosenselect', {
  props: {
    label: String,
    placeholder: { type: String, default: 'Select one' },
    hasIcon: { type: Boolean, default: false },
    isMultiple: { type: Boolean, default: false },
    options: { type: Array }
  },
  template: `
    <div class="form-group">
      <label v-if="label">
        {{ label }}
        <i v-if="hasIcon" class="fa fa-info-circle" style="color:#53688E;" data-toggle="tooltip" data-placement="right" title="Info"></i> 
      </label>
      <select class="chosen-select" :data-placeholder="placeholder" :multiple="isMultiple" tabindex="4">
        <option v-for="option in options" :value="option.value">
          {{ option.text }}
        </option>
      </select>
    </div>
  `
});