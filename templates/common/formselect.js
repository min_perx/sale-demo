Vue.component('formselect', {
  props: {
    label: String,
    placeholder: { type: String, default: 'Please select' },
    hasIcon: { type: Boolean, default: false },
    options: { type: Array }
  },
  template: `
    <div class="form-group">
      <label v-if="label">
        {{ label }}
        <i v-if="hasIcon" class="fa fa-info-circle" style="color:#53688E;" data-toggle="tooltip" data-placement="right" title="Info"></i> 
      </label>
      <select class="select2_demo_2 form-control" :data-placeholder="placeholder">
        <option v-for="option in options" :value="option.value">
          {{ option.text }}
        </option>
      </select>
    </div>
  `
});