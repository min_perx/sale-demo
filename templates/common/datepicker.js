Vue.component('datepicker', {
  props: {
    label: String,
    placeholder: { type: String, default: 'Pick a date' },
    hasIcon: { type: Boolean, default: false }
  },
  template: `
    <div class="form-group">
      <label v-if="label">
        {{ label }}
        <i v-if="hasIcon" class="fa fa-info-circle" style="color:#53688E;" data-toggle="tooltip" data-placement="right" title="Info"></i> 
      </label>
      <div class="input-group-icon input-group-left-icon">
        <input type="text" class="datepicker form-control" :placeholder="placeholder">
        <div class="input-left-icon">
          <i class="fa fa-calendar"></i>
        </div>
      </div>
    </div>
  `
});
