Vue.component('textfield', {
  props: {
    label: String,
    placeholder: { type: String, default: 'Type something here'},
    hasIcon: { type: Boolean, default: false }
  },
  template: `
    <div class="form-group">
      <label v-if="label">
        {{ label }}
        <i v-if="hasIcon" class="fa fa-info-circle" style="color:#53688E;" data-toggle="tooltip" data-placement="right" title="Info"></i> 
      </label>
      <input type="text" class="form-control" :placeholder="placeholder">
    </div>
  `
});