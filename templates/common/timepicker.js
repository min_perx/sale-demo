Vue.component('timepicker', {
  props: {
    label: String,
    placeholder: { type: String, default: 'Pick a time' },
    hasIcon: { type: Boolean, default: false }
  },
  template: `
    <div class="form-group">
      <label v-if="label">
        {{ label }}
        <i v-if="hasIcon" class="fa fa-info-circle" style="color:#53688E;" data-toggle="tooltip" data-placement="right" title="Info"></i> 
      </label>
      <div class="input-group-icon input-group-right-icon">
        <input type="text" class="clockpicker form-control" data-autoclose="true" :placeholder="placeholder">
        <div class="input-right-icon">
          <i class="fa fa-clock-o"></i>
        </div>
      </div>
    </div>
  `
});