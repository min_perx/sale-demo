Vue.component('navbar', {
  template: `
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                  <div class="profile-element">
                    <div class="col-sm-12 col-md-12 col-lg-12 nobspadding">
                      <img alt="image" class="logo-space" src="img/logo.png" />
                    </div>
                    <div class="space-60"> </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 nobspadding">
                      <div class="col-sm-12 col-md-5 col-lg-5 nobspadding">
                        <span><img alt="image" class="img-circle" src="img/profile_small.jpg" /></span>
                      </div>
                      <div class="col-sm-12 col-md-7 col-lg-7 nobspadding">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold text-white">Anna Gong</strong>
                        </span> <span class="text-xs text-white block">Administrator</span> </span>
                      </div>
                    </div>
                  </div>
                  <div class="logo-element">
                    PERX
                  </div>
                </li>
                <li>
                    <a href="index.html"><i class="fa fa-line-chart"></i> <span class="nav-label">Dashboard</span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-pie-chart"></i> <span class="nav-label">Business insights</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="business_insights.html">Insights</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-bullhorn"></i> <span class="nav-label">Campaigns</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="campaigns.html">List</a></li>
                        <li><a href="create_loyalty.html">Loyalty Programs</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-gift"></i> <span class="nav-label">Rewards</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Reward 1</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-list-alt"></i> <span class="nav-label">List Management</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">List 1</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Users</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">User 1</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-comments-o"></i> <span class="nav-label">Customer Service</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">CS 1</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Settings</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Setting 1</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>
  `
})