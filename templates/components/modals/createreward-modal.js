Vue.component('createreward-modal', {
  template: `
    <div class="modal inmodal" id="createreward" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
          <div class="modal-body tooltip-demo">
            <div class="row">
              <div class="space-30"> </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                <table>
                  <tr>
                    <td class="padding-right-5">
                      <h2 class="no-margins padding-title">Create Reward</h2>
                    </td>
                    <td><i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i></td>
                  </tr>
                </table>
                <br><br>
              </div>
            </div>
            <div class="row" style="margin-left:0 !important;">
              <div class="col-lg-8">
                <div class="listmanagement-borderbox">
                  <div class="ibox no-border no-margins">
                    <div class="ibox-content no-border">
                      <div class="form-group">
                        <label>
                        Reward Name
                        <i class="fa fa-info-circle" style="color:#53688E;" data-toggle="tooltip" data-placement="right" title="Info"></i> 
                        </label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end of listmanagement-borderbox -->
                <div class="listmanagement-borderbox">
                  <div class="ibox no-border no-margins">
                    <div class="ibox-content no-border">
                      <div class="form-group">
                        <label>
                        Reward Description
                        <i class="fa fa-info-circle" style="color:#53688E;" data-toggle="tooltip" data-placement="right" title="Info"></i> 
                        </label>
                        <textarea class="form-control" rows="3" style="resize:none;"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end of listmanagement-borderbox -->
                <div class="listmanagement-borderbox">
                  <div class="ibox no-border no-margins">
                    <div class="ibox-content no-border">
                      <h4>
                        Duration <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                      </h4>
                      <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-6">
                          <div class="form-group">
                            <label>Start date</label>
                            <div class="input-group-icon input-group-left-icon">
                              <input type="text" class="datepicker form-control" value="03/04/2014">
                              <div class="input-left-icon">
                                <i class="fa fa-calendar"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                          <div class="form-group">
                            <label>End date</label>
                            <div class="input-group-icon input-group-left-icon">
                              <input type="text" class="datepicker form-control" value="03/04/2014">
                              <div class="input-left-icon">
                                <i class="fa fa-calendar"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end of listmanagement-borderbox -->
                <div class="listmanagement-borderbox">
                  <div class="ibox no-border no-margins">
                    <div class="ibox-content no-border">
                      <h4>
                        Reward Value <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                      </h4>
                      <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4">
                          <div class="form-group">
                            <label>Currency</label>
                            <select class="select2_demo_2 form-control">
                              <option value="1">SGD</option>
                              <option value="2">USD</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4">
                          <div class="form-group">
                            <label>Amount</label>
                            <input type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4">
                          <div class="form-group">
                            <label>Quota</label>
                            <input type="text" class="form-control">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end of listmanagement-borderbox -->
                <div class="listmanagement-borderbox">
                  <div class="ibox no-border no-margins">
                    <div class="ibox-content no-border">
                      <h4>
                        User Limits(Optional) <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                      </h4>
                      <div class="form-horizontal">
                        <div class="form-group">
                          <input type="checkbox" class="reward-user-limit-switch col-sm-3"  />
                          <label class="col-sm-3 control-label">Disabled</label>
                        </div>
                        <div id="reward-user-limit-switch-section" style="display:none;">
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Max. Issuable in Total</label>
                            <div class="col-sm-9"><input type="text" class="form-control"></div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Max. Issuable Per Interval</label>
                            <div class="col-sm-9">
                              <div class="row">
                                <div class="col-sm-3">
                                  <input type="text" class="form-control">
                                </div>
                                <div class="col-sm-3">
                                  <select class="select2_demo_1 form-control">
                                    <option value="1">Every</option>
                                    <option value="2">Within</option>
                                  </select>
                                </div>
                                <div class="col-sm-3">
                                  <input type="text" class="form-control">
                                </div>
                                <div class="col-sm-3">
                                  <select class="select2_demo_2 form-control">
                                    <option value="1">minute</option>
                                    <option value="2">hour</option>
                                    <option value="3">day</option>
                                    <option value="4">week</option>
                                    <option value="5">month</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end of listmanagement-borderbox -->
                <div class="listmanagement-borderbox">
                  <div class="ibox no-border no-margins">
                    <div class="ibox-content no-border">
                      <h4>
                        Category <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                      </h4>
                      <div class="form-group">
                        <select data-placeholder="Select Type" class="chosen-select" tabindex="2">
                          <option value="Catalogue">Catalogue</option>
                          <option value="Coupon">Coupon</option>
                          <option value="Survey">Survey</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end of listmanagement-borderbox -->
                <div class="listmanagement-borderbox">
                  <div class="ibox no-border no-margins">
                    <div class="ibox-content no-border">
                      <div class="form-group">
                        <label>
                        Steps to Redeem
                        <i class="fa fa-info-circle" style="color:#53688E;" data-toggle="tooltip" data-placement="right" title="Info"></i> 
                        </label>
                        <textarea class="form-control" rows="3" style="resize:none;" placeholder="Type the steps on how to redeem your reward"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end of listmanagement-borderbox -->
                <div class="listmanagement-borderbox">
                  <div class="ibox no-border no-margins">
                    <div class="ibox-content no-border">
                      <div class="form-group">
                        <label>
                        Terms and Conditions
                        <i class="fa fa-info-circle" style="color:#53688E;" data-toggle="tooltip" data-placement="right" title="Info"></i> 
                        </label>
                        <textarea class="form-control" rows="3" style="resize:none;" placeholder="Type the terms and conditions"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end of listmanagement-borderbox -->
              </div>
              <div class="col-lg-4">
                <div class="listmanagement-borderbox">
                  <div class="ibox no-border no-margins">
                    <div class="ibox-content no-border">
                      <h4>
                        Upload Reward Photo <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                      </h4>
                      <img src="https://placehold.it/200x150" width="100%" height="150">
                      <p class="text-center">
                        <a href="#">Upload image</a>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="listmanagement-borderbox">
                  <div class="ibox no-border no-margins">
                    <div class="ibox-content no-border">
                      <h4>
                        Redemption Type <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                      </h4>
                      <select data-placeholder="Choose category" class="chosen-select"  tabindex="2">
                        <option selected value="Offline">Offline</option>
                        <option  value="Online">Online</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="listmanagement-borderbox">
                  <div class="ibox no-border no-margins">
                    <div class="ibox-content no-border">
                      <h4>
                        Voucher Fulfilment Code <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                      </h4>
                      <select data-placeholder="Choose category" class="chosen-select"  tabindex="2">
                        <option selected value="promo-code">Promo Code</option>
                      </select>
                      <div class="radio radio-primary">
                        <input type="radio" name="code" id="system-radio" value="system">
                        <label for="system-radio">
                        System Generated
                        </label>
                      </div>
                      <div class="radio radio-primary">
                        <input type="radio" name="code" id="manual-radio" value="manual">
                        <label for="manual-radio">
                        Manual Encode
                        </label>
                      </div>
                      <div class="radio radio-primary">
                        <input type="radio" name="code" id="csv-radio" value="csv">
                        <label for="csv-radio">
                        CSV Upload
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="listmanagement-borderbox">
                  <div class="ibox no-border no-margins">
                    <div class="ibox-content no-border">
                      <h4>
                        Voucher Timer <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                      </h4>
                      <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4">
                          <div class="form-group">
                            <label>Hour</label>
                            <input type="number" class="form-control">
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4">
                          <div class="form-group">
                            <label>Minute</label>
                            <input type="number" class="form-control">
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4">
                          <div class="form-group">
                            <label>Second</label>
                            <input type="number" class="form-control">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end of listmanagement-borderbox -->
              </div>
            </div>
          </div>
          <div class="modal-footer modal-footer-reward">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal">Save Reward</button>
          </div>
        </div>
      </div>
    </div>
  `
})