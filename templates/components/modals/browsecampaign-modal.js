Vue.component('browsecampaign-modal', {
  template: `
    <div class="modal inmodal" id="browsecampaigns" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
          <div class="modal-body">
            <div class="row">
              <div class="space-30"> </div>
              <div class="col-sm-12 col-md-12 col-lg-12 space-60 tooltip-demo">
                <table>
                  <tr>
                    <td class="padding-right-20">
                      <h2 class="no-margins">Campaigns</h2>
                    </td>
                  </tr>
                </table>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="col-sm-5">
                    <input type="text" placeholder="Find a campaign by name or type" class="input-sm form-control" style="max-width: 408px;">
                  </div>
                  <div class="col-sm-2 m-b-xs">
                  </div>
                  <div class="col-sm-5">
                    <table class="pull-right">
                      <tr>
                        <td class="padding-right-20">Sort by</td>
                        <td width="80%">
                          <select class="input-sm form-control input-s-sm inline">
                            <option value="0">Last Updated</option>
                          </select>
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 borderbox-padding">
              <div class="tabs-container tabs-container-list">
                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#campaign-all">All (5)</a></li>
                  <li class=""><a data-toggle="tab" href="#campaign-recent">Latest (2)</a></li>
                  <li class=""><a data-toggle="tab" href="#campaign-unassigned">Unassigned (2)</a></li>
                  <li class=""><a data-toggle="tab" href="#campaign-draft">Draft (0)</a></li>
                </ul>
                <div class="tab-content">
                  <div id="campaign-all" class="tab-pane active">
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th></th>
                              <th style="width:50%">Name</th>
                              <th style="width:25%">Owner</th>
                              <th style="width:25%">Owner</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td><i class="fa fa-circle text-navy circle"></i> <strong>30% off 4K Ultra HD Smart LED</strong> <label class="label label-warning">NEW</label>
                              </td>
                              <td>XYZ Appliance Center</td>
                              <td>Stamp</td>
                            </tr>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td><i class="fa fa-circle text-navy circle"></i> <strong>30% off 4K Ultra HD Smart LED</strong> <label class="label label-warning">NEW</label>
                              </td>
                              <td>XYZ Appliance Center</td>
                              <td>Stamp</td>
                            </tr>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td><i class="fa fa-circle text-navy circle"></i> <strong>30% off 4K Ultra HD Smart LED</strong> <label class="label label-warning">NEW</label>
                              </td>
                              <td>XYZ Appliance Center</td>
                              <td>Stamp</td>
                            </tr>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td><i class="fa fa-circle text-navy circle"></i> <strong>30% off 4K Ultra HD Smart LED</strong> <label class="label label-warning">NEW</label>
                              </td>
                              <td>XYZ Appliance Center</td>
                              <td>Stamp</td>
                            </tr>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td><i class="fa fa-circle text-navy circle"></i> <strong>30% off 4K Ultra HD Smart LED</strong> <label class="label label-warning">NEW</label>
                              </td>
                              <td>XYZ Appliance Center</td>
                              <td>Stamp</td>
                            </tr>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td><i class="fa fa-circle text-navy circle"></i> <strong>30% off 4K Ultra HD Smart LED</strong> <label class="label label-warning">NEW</label>
                              </td>
                              <td>XYZ Appliance Center</td>
                              <td>Stamp</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="campaign-recent" class="tab-pane">
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th></th>
                              <th style="width:50%">Name</th>
                              <th style="width:25%">Owner</th>
                              <th style="width:25%">Type</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td><i class="fa fa-circle text-navy circle"></i> <strong>30% off 4K Ultra HD Smart LED</strong></td>
                              <td>XYZ Appliance Center</td>
                              <td>Stamp</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="campaign-unassigned" class="tab-pane">
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th></th>
                              <th style="width:50%">Name</th>
                              <th style="width:25%">Owner</th>
                              <th style="width:25%">Type</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td><i class="fa fa-circle text-navy circle"></i> <strong>30% off 4K Ultra HD Smart LED</strong></td>
                              <td>XYZ Appliance Center</td>
                              <td>Stamp</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="campaign-draft" class="tab-pane">
                    <div class="panel-body">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer no-border">
            <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal">Add Campaign</button>
          </div>
        </div>
      </div>
    </div>
  `
});