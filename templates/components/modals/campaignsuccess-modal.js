Vue.component('campaignsuccess-modal', {
  template: `
    <div class="modal inmodal" id="campaignSuccess" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
          <div class="modal-body text-center">
            <h2>Good Job!</h2>
            <img src="http://placehold.it/150x150" class="img-circle">
            <p>Your Campaign is now launched!</p>
            <a href="campaigns.html" class="btn btn-primary">View all campaigns</a>
          </div>
        </div>
      </div>
    </div>
  `
})