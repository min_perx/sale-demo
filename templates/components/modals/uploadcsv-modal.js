Vue.component('uploadcsv-modal', {
  template: `
    <div class="modal inmodal" id="uploadcsv" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
          <div class="modal-header no-border">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Upload CSV</h4>
          </div>
          <div class="modal-body">
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
              <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
              <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span>
              <input type="file" name="...">
              </span>
              <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
            </div>
          </div>
          <div class="modal-footer no-border">
            <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal">Upload</button>
          </div>
        </div>
      </div>
    </div>
  `
});