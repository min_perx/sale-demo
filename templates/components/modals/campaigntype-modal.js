Vue.component('campaigntype-modal', {
  template: `
    <div class="modal inmodal" id="campaigntype" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
                <div class="modal-header no-border">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">What is the name of your new campaign?</h4>
                </div>
                <div class="modal-body">
                <div class="form-group">
                <h4 class="pull-left font-400">Campaign name</h4>
                <input type="text" class="form-control" placeholder="eg. Campaign 1" required="">
                </div>                                        
                </div>
                <div class="modal-footer no-border">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Back</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#campaigntype">Save and Next</button>
                </div>
            </div>
        </div>
    </div>
  `
});