Vue.component('browsereward-modal', {
  template: `
    <div class="modal inmodal" id="browserewards" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
          <div class="modal-body">
            <div class="row">
              <div class="space-30"> </div>
              <div class="col-sm-12 col-md-12 col-lg-12 space-60 tooltip-demo">
                <table>
                  <tr>
                    <td class="padding-right-20">
                      <h2 class="no-margins">Rewards</h2>
                    </td>
                    <td> 
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createreward">Create Reward</button>
                    </td>
                  </tr>
                </table>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="col-sm-5">
                    <input type="text" placeholder="Find a campaign by name or type" class="input-sm form-control" style="max-width: 408px;">
                  </div>
                  <div class="col-sm-2 m-b-xs">
                  </div>
                  <div class="col-sm-5">
                    <table class="pull-right">
                      <tr>
                        <td class="padding-right-20">Sort by</td>
                        <td width="80%">
                          <select class="input-sm form-control input-s-sm inline">
                            <option value="0">Last Updated</option>
                          </select>
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 borderbox-padding">
              <div class="tabs-container tabs-container-list">
                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#reward-all">All (5)</a></li>
                  <li class=""><a data-toggle="tab" href="#reward-recent">Latest (2)</a></li>
                  <li class=""><a data-toggle="tab" href="#reward-unassigned">Unassigned (2)</a></li>
                  <li class=""><a data-toggle="tab" href="#reward-draft">Draft (0)</a></li>
                </ul>
                <div class="tab-content">
                  <div id="reward-all" class="tab-pane active">
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th></th>
                              <th style="width:6%">ID</th>
                              <th style="width:34%">Name</th>
                              <th style="width:15%">Category</th>
                              <th style="width:15%">Date</th>
                              <th style="width:15%">Owner</th>
                              <th style="width:15%" class="text-center">Status</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td>3828</td>
                              <td><i class="fa fa-circle text-navy circle"></i> <strong>30% off 4K Ultra HD Smart LED</strong> <label class="label label-warning">NEW</label>
                              </td>
                              <td>Consumer Electronics</td>
                              <td>
                                08/07/2017
                              </td>
                              <td>XYZ Appliance Center</td>
                              <td class="text-center">
                                <a class="btn btn-primary" href="#"><i class="fa fa-plus padding-right-5"></i> Assign to Campaign</a>                                       
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td>683</td>
                              <td><i class="fa fa-circle text-navy circle"></i> <strong>20% Off Full HD Smart LED TV</strong> <label class="label label-warning">NEW</label></td>
                              <td>Consumer Electronics</td>
                              <td>
                                08/07/2017
                              </td>
                              <td>XYZ Appliance Center</td>
                              <td class="text-center">
                                <a class="btn btn-primary" href="#"><i class="fa fa-plus padding-right-5"></i> Assign to Campaign</a>                                       
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td>1090</td>
                              <td><i class="fa fa-circle text-muted circle"></i>Free 1,000 gift item</td>
                              <td>Shopping</td>
                              <td>
                                01/02/2017
                              </td>
                              <td>Paragon</td>
                              <td class="text-center">
                                <span class="btn btn-w-m btn-default full-width">Assigned</span>                                       
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td>987</td>
                              <td><i class="fa fa-circle text-muted circle"></i>30% off bill</td>
                              <td>Dining</td>
                              <td>
                                06/02/2017
                              </td>
                              <td>KFC</td>
                              <td class="text-center">
                                <span class="btn btn-w-m btn-default full-width">Assigned</span>                                       
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td>820</td>
                              <td><i class="fa fa-circle text-muted circle"></i>500 Points Free Dinner</td>
                              <td>Dining</td>
                              <td>
                                04/12/2016
                              </td>
                              <td>Bergs</td>
                              <td class="text-center">
                                <span class="btn btn-w-m btn-default full-width">Assigned</span>                                       
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="reward-recent" class="tab-pane">
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th></th>
                              <th style="width:6%">ID</th>
                              <th style="width:34%">Name</th>
                              <th style="width:15%">Category</th>
                              <th style="width:15%">Date</th>
                              <th style="width:15%">Owner</th>
                              <th style="width:15%" class="text-center">Status</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td>3828</td>
                              <td><i class="fa fa-circle text-navy circle"></i> <strong>30% off 4K Ultra HD Smart LED</strong></td>
                              <td>Consumer Electronics</td>
                              <td>
                                08/07/2017
                              </td>
                              <td>XYZ Appliance Center</td>
                              <td class="text-center">
                                <a class="btn btn-primary" href="#"><i class="fa fa-plus padding-right-5"></i> Assign to Campaign</a>                                       
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="reward-unassigned" class="tab-pane">
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th></th>
                              <th style="width:6%">ID</th>
                              <th style="width:34%">Name</th>
                              <th style="width:15%">Category</th>
                              <th style="width:15%">Date</th>
                              <th style="width:15%">Owner</th>
                              <th style="width:15%" class="text-center">Status</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td>3828</td>
                              <td><i class="fa fa-circle text-navy circle"></i> <strong>30% off 4K Ultra HD Smart LED</strong></td>
                              <td>Consumer Electronics</td>
                              <td>
                                08/07/2017
                              </td>
                              <td>XYZ Appliance Center</td>
                              <td class="text-center">
                                <a class="btn btn-primary" href="#"><i class="fa fa-plus padding-right-5"></i> Assign to Campaign</a>                                       
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div class="checkbox checkbox-primary">
                                  <input type="checkbox">
                                  <label>
                                  </label>
                                </div>
                              </td>
                              <td>683</td>
                              <td><i class="fa fa-circle text-navy circle"></i> <strong>20% Off Full HD Smart LED TV</strong></td>
                              <td>Consumer Electronics</td>
                              <td>
                                08/07/2017
                              </td>
                              <td>XYZ Appliance Center</td>
                              <td class="text-center">
                                <a class="btn btn-primary" href="#"><i class="fa fa-plus padding-right-5"></i> Assign to Campaign</a>                                       
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="reward-draft" class="tab-pane">
                    <div class="panel-body">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer no-border">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  `
});