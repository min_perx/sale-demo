Vue.component('createaudience-modal', {
  template: `
    <div class="modal inmodal" id="createaudience" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
          <div class="modal-body tooltip-demo">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <h2>
                  Create Audience 
                </h2>
                </h4>
                  Select the location, age, gender and interests of people you want to target.
                </h4>
                <div class="space-60"></div>
              </div>
            </div>
            <div class="row" style="margin-left:0 !important;">
              <div class="col-lg-12">
                <div class="listmanagement-borderbox">
                  <div class="ibox no-border no-margins">
                    <div class="ibox-content no-border">
                      <textfield 
                        label="Audience name"
                      />
                      <div class="form-group">
                        <label>Gender</label>
                        <br>
                        <div class="btn-group" data-toggle="buttons">
                          <label class="btn btn-default">
                            <input type="radio" name="genderoptions" id="option1" autocomplete="off">
                            Male
                          </label>
                          <label class="btn btn-default">
                            <input type="radio" name="genderoptions" id="option2" autocomplete="off">
                            Female
                          </label>
                          <label class="btn btn-default">
                            <input type="radio" name="genderoptions" id="option3" autocomplete="off">
                            Both
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Age</label>
                        <div class="form-inline">
                          <formselect
                            :options="[
                              { value: '18', text: '18' },
                              { value: '19', text: '19' },
                              { value: '20', text: '20' },
                              { value: '21', text: '21' },
                              { value: '22', text: '22' },
                              { value: '23', text: '23' },
                              { value: '24', text: '24' },
                              { value: '25', text: '25' },
                              { value: '26', text: '26' },
                              { value: '27', text: '27' },
                              { value: '28', text: '28' }
                            ]"
                          />
                          -
                          <formselect
                            :options="[
                              { value: '18', text: '18' },
                              { value: '19', text: '19' },
                              { value: '20', text: '20' },
                              { value: '21', text: '21' },
                              { value: '22', text: '22' },
                              { value: '23', text: '23' },
                              { value: '24', text: '24' },
                              { value: '25', text: '25' },
                              { value: '26', text: '26' },
                              { value: '27', text: '27' },
                              { value: '28', text: '28' }
                            ]"
                          />
                        </div>
                      </div>
                      <textfield label="Locations" />
                      <textfield label="Product Category Targeting" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  `
});