Vue.component('loyaltymechanic-form', {
  template: `
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Campaign Limits(Optional) <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="form-horizontal">
                <div class="form-group">
                  <input type="checkbox" class="loyalty-campaign-limit-switch col-sm-3"  />
                  <label class="col-sm-3 control-label">Disabled</label>
                </div>
                <div id="loyalty-campaign-limit-switch-section" style="display:none;">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Max. Issuable in Total</label>
                    <div class="col-sm-9"><input type="text" class="form-control"></div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Max. Issuable Per Interval</label>
                    <div class="col-sm-9">
                      <div class="row">
                        <div class="col-sm-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-3">
                          <select class="select2_demo_1 form-control">
                            <option value="1">Every</option>
                            <option value="2">Within</option>
                          </select>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-3">
                          <select class="select2_demo_2 form-control">
                            <option value="1">minute</option>
                            <option value="2">hour</option>
                            <option value="3">day</option>
                            <option value="4">week</option>
                            <option value="5">month</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Customer Limits(Optional) <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="form-horizontal">
                <div class="form-group">
                  <input type="checkbox" class="loyalty-customer-limit-switch col-sm-3"  />
                  <label class="col-sm-3 control-label">Disabled</label>
                </div>
                <div id="loyalty-customer-limit-switch-section" style="display:none;">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Max. Issuable in Total</label>
                    <div class="col-sm-9"><input type="text" class="form-control"></div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Max. Issuable Per Interval</label>
                    <div class="col-sm-9">
                      <div class="row">
                        <div class="col-sm-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-3">
                          <select class="select2_demo_1 form-control">
                            <option value="1">Every</option>
                            <option value="2">Within</option>
                          </select>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-3">
                          <select class="select2_demo_2 form-control">
                            <option value="1">minute</option>
                            <option value="2">hour</option>
                            <option value="3">day</option>
                            <option value="4">week</option>
                            <option value="5">month</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Loyalty program selection <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <textfield />
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <a href="#">Browse Program</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Membership Tier (Optional)<i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="form-inline m-b-md">
                    <div class="form-group m-r-md">
                      <label>Tier1</label>
                    </div>
                    <div class="form-group m-r-sm">
                      <input type="text" class="form-control" />
                    </div>
                    <div class="form-group m-r-sm">
                      <input type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                      <label>points</label>
                    </div>
                  </div>
                  <div class="form-inline m-b-md">
                    <div class="form-group m-r-md">
                      <label>Tier2</label>
                    </div>
                    <div class="form-group m-r-sm">
                      <input type="text" class="form-control" />
                    </div>
                    <div class="form-group m-r-sm">
                      <input type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                      <label>points</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <a href="#">
                    <i class="fa fa-plus"></i> Add Tier
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Value Per Point <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="form-inline m-b-md">
                    <div class="form-group m-r-xl">
                      <label>Tier1</label>
                    </div>
                    <div class="form-group m-r-sm">
                      <label>$</label>
                    </div>
                    <div class="form-group m-r-sm">
                      <input type="text" class="form-control" />
                    </div>
                    <div class="form-group m-r-sm">
                      <label>equals</label>
                    </div>
                    <div class="form-group m-r-sm">
                      <input type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                      <label>points</label>
                    </div>
                  </div>
                  <div class="form-inline m-b-md">
                    <div class="form-group m-r-xl">
                      <label>Tier2</label>
                    </div>
                    <div class="form-group m-r-sm">
                      <label>$</label>
                    </div>
                    <div class="form-group m-r-sm">
                      <input type="text" class="form-control" />
                    </div>
                    <div class="form-group m-r-sm">
                      <label>equals</label>
                    </div>
                    <div class="form-group m-r-sm">
                      <input type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                      <label>points</label>
                    </div>
                  </div>
                  <div class="form-inline m-b-md">
                    <div class="form-group m-r-xl">
                      <label>Tier3</label>
                    </div>
                    <div class="form-group m-r-sm">
                      <label>$</label>
                    </div>
                    <div class="form-group m-r-sm">
                      <input type="text" class="form-control" />
                    </div>
                    <div class="form-group m-r-sm">
                      <label>equals</label>
                    </div>
                    <div class="form-group m-r-sm">
                      <input type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                      <label>points</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <a href="#">
                    <i class="fa fa-plus"></i> Add Tier
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Custom Field <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <textfield />
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <a href="#">Add Custom Field</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
});