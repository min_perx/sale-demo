Vue.component('loyaltyreview', {
  template: `
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-12">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>Campaign Info</h4>
              <div class="row">
                <div class="col-md-12">
                  <div class="profile-image">
                    <img src="img/a4.jpg" class="m-b-md" alt="profile">
                  </div>
                  <div class="profile-info">
                    <div class="">
                      <div>
                        <h2 class="no-margins">
                          The One Card Loyalty Program
                        </h2>
                        <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                        <div class="space-60"></div>
                        <p>
                          <small>
                          <i class="fa fa-calendar padding-right-5 text-primary"></i> starts: 21/08/2017 ends: 21/09/2017
                          </small>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-12">
                  <h4>Mechanics Setup</h4>

                  <dl class="dl-horizontal m-t-md large">
                    <dt>Campaign Limits:</dt>
                    <dd>Enabled</dd>
                    <dt>Customer Limits:</dt>
                    <dd>Enabled</dd>
                    <dt>Loyalty Program Selected:</dt>
                    <dd>The Central Group Loyalty Program</dd>
                  </dl>
                  <p> Membership Tier: </p>
                  <dl class="dl-horizontal m-t-md large">
                    <dt>Gold:</dt>
                    <dd>100 points</dd>
                    <dt>Silver:</dt>
                    <dd>50 points</dd>
                  </dl>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Event Processor CSV Upload
                <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="space-30"></div>
              <div class="form-group">
                <label>Upload CSV (Download Sample File)</label>
                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                  <div class="form-control" data-trigger="fileinput">
                      <i class="glyphicon glyphicon-file fileinput-exists"></i>
                  <span class="fileinput-filename"></span>
                  </div>
                  <span class="input-group-addon btn btn-default btn-file">
                      <span class="fileinput-new">Select file</span>
                      <span class="fileinput-exists">Change</span>
                      <input type="file" name="..."/>
                  </span>
                  <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                </div> 
              </div>
              <div class="form-group text-right">
                <button class="btn btn-primary">Upload File</button>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})