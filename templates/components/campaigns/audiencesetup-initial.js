Vue.component('audiencesetup-initial', {
  template: `
    <div id="audiencesetupInitial">
      <div class="row text-center">
        <h3>Kindly select your desired Audience Setup</h3>
      </div>
      <div class="space-60"></div>
      <div class="row">
        <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
          <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-12">
              <div id="audiencesetupAuto" class="widget audiencesetup-widget p-lg text-center" onclick="selectAudienceSetup(this.id)">
                  <div class="m-b-md">
                      <i class="fa fa-magic fa-3x"></i>
                      <h3 class="font-bold no-margins">
                          Automatic
                      </h3>
                      <p>Our recommendation engine will help you select your target audience automatically</p>
                  </div>
              </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12">
              <div id="audiencesetupManual" class="widget audiencesetup-widget p-lg text-center" onclick="selectAudienceSetup(this.id)">
                  <div class="m-b-md">
                      <i class="fa fa-cog fa-3x"></i>
                      <h3 class="font-bold no-margins">
                        Manual
                      </h3>
                      <p>Start  customizing your desired target audience with full control</p>
                  </div>
              </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12">
              <div id="audiencesetupCSV" class="widget audiencesetup-widget p-lg text-center" onclick="selectAudienceSetup(this.id)">
                  <div class="m-b-md">
                      <i class="fa fa-upload fa-3x"></i>
                      <h3 class="font-bold no-margins">
                        Upload CSV
                      </h3>
                      <p>Already have your own list?</p>
                      <p>Upload your own audience list seamlessly</p>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
});