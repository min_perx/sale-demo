Vue.component('surveymechanic-form', {
  props: {
    id: String,
    display: { type: String, default: 'block' }
  },
  template: `
    <div :id="id" class="row" :style="{ display: [display] }">
      <div class="col-lg-8 col-sm-12 col-md-8">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Multiple Choice Questions <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="form-group">
                <label>Question 1</label>
                <input type="text" class="form-control" placeholder="Enter your question">
              </div>
              <div class="form-group">
                <label>Answer Choices</label>
                <div class="row answer-row">
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Enter an answer choice">
                  </div>
                  <div class="col-sm-2">
                    <button class="btn btn-default btn-circle btn-outline" type="button">
                      <i class="fa fa-plus"></i>
                    </button>
                    <button class="btn btn-default btn-circle btn-outline" type="button">
                      <i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div class="row answer-row">
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Enter an answer choice">
                  </div>
                  <div class="col-sm-2">
                    <button class="btn btn-default btn-circle btn-outline" type="button">
                      <i class="fa fa-plus"></i>
                    </button>
                    <button class="btn btn-default btn-circle btn-outline" type="button">
                      <i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div class="row answer-row">
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Enter an answer choice">
                  </div>
                  <div class="col-sm-2">
                    <button class="btn btn-default btn-circle btn-outline" type="button">
                      <i class="fa fa-plus"></i>
                    </button>
                    <button class="btn btn-default btn-circle btn-outline" type="button">
                      <i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <button type="button" class="btn btn-w-m btn-default">
                  <i class="fa fa-plus-circle"></i> Add Question
                </button>
                <div class="pull-right">
                  <button type="button" class="btn btn-w-m btn-default">
                    Cancel
                  </button>
                  <button type="button" class="btn btn-w-m btn-primary">
                    Save
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-12 col-md-4">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Code Triggers <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <p>Promo Code(Optional)</p>
              <div class="form-horizontal">
                <div class="form-group">
                  <input type="checkbox" class="survey-promo-code-switch col-sm-3"  />
                  <label class="col-sm-3 control-label">Disabled</label>
                </div>
              </div>
              <p>Parameters</p>
              <div class="form-group">
                <label>Total scans</label>
                <input type="text" class="form-control">
              </div>
              <div class="form-group">
                <label>Total user served</label>
                <input type="text" class="form-control">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
});