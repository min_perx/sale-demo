Vue.component('audiencesetup-csv-form', {
  props: {
    id: String,
    display: { type: String, default: 'block' }
  },
  template: `
    <div :id="id" :style="{ display: [display] }">
      <div class="row">
        <div class="col-md-12">
          <ol class="breadcrumb">
            <li>Audience Setup</li>
            <li class="active">Upload CSV</li>
          </ol>
        </div>
      </div>
      <div id="audiencesetup-csv-form" class="row" style="display:none;">
        <div class="col-md-6 col-md-offset-3">
          <div class="listmanagement-borderbox">
            <div class="ibox no-border no-margins">
              <div class="ibox-content no-border">
                <h4>
                  Import from CSV file
                </h4>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="radio radio-primary">
                      <input name="csvlist" id="whitelistRadio" type="radio">
                      <label for="whitelistRadio">
                        Whitelist 
                        <i data-toggle="tooltip" data-placement="right" title="Info" class="fa fa-info-circle text-grey"></i>
                      </label>
                    </div>
                    <div class="radio radio-primary">
                      <input name="csvlist" id="blacklistRadio" type="radio">
                      <label for="blacklistRadio">
                        Blacklist 
                        <i data-toggle="tooltip" data-placement="right" title="Info" class="fa fa-info-circle text-grey"></i>
                      </label>
                    </div>
                    <form action="#" class="dropzone" id="dropzoneForm">
                      <div class="fallback">
                          <input name="file" type="file" multiple />
                      </div>
                  </form> 
                  </div>
                </div>
                <div class="space-60"></div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <button type="button" class="btn btn-default" onclick="showInitial()">Back</button> 
                    <button type="button" class="btn btn-primary" onclick="goNextStep()">Save and Next</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
});