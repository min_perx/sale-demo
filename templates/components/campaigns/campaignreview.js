Vue.component('campaignreview', {
  template: `
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-12">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>Campaign Info</h4>
              <div class="row">
                <div class="col-md-12">
                  <div class="profile-image">
                    <img src="img/a4.jpg" class="m-b-md" alt="profile">
                  </div>
                  <div class="profile-info">
                    <div class="">
                      <div>
                        <h2 class="no-margins">
                          Stock Clearance Sony HDTVs
                        </h2>
                        <h4>Redeem your TV discount voucher now</h4>
                        <div class="space-30"></div>
                        <p>
                          <small>
                          <i class="fa fa-calendar padding-right-5 text-primary"></i> starts: 21/08/2017 ends: 21/09/2017
                          </small>
                        </p>
                        <p>
                          <small>
                          <i class="fa fa-users padding-right-5 text-primary"></i> 25-45 Male from Bangkok (1.656 Customers)
                          </small>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-12">
                  <h4>Mechanics Setup</h4>
                  <div class="media">
                    <div class="media-left media-middle">
                      <a href="#"> <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="https://placehold.it/150x150" data-holder-rendered="true" style="width: 70px; height: 70px;"> </a> 
                    </div>
                    <div class="media-body p-h-lg">
                      <h4 class="media-heading">Stamp Mechanic</h4>
                    </div>
                  </div>
                </div>
              </div>
              <div class="space-30"></div>
              <div class="row">
                <div class="col-md-12">
                  <h4>Triggers</h4>
                  <div class="media">
                    <div class="media-left media-middle">
                      <a href="#"> <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="https://placehold.it/150x150" data-holder-rendered="true" style="width: 70px; height: 70px;"> </a> 
                    </div>
                    <div class="media-body p-h-lg">
                      <h4 class="media-heading">Trigger a Voucher</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Added Rewards
                <a href="#" class="pull-right">View All</a>
              </h4>
              <div class="space-30"></div>
              <h4>20 points to trigger Voucher</h4>
              <div class="media">
                <div class="media-left media-middle">
                  <a href="#"> <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="https://placehold.it/150x150" data-holder-rendered="true" style="width: 70px; height: 70px;"> </a> 
                </div>
                <div class="media-body p-h-xs">
                  <p class="m-n">35% Off Full HD Smart LED TV 43"</p>
                  <p class="m-n">qty: 72 of 300</p>
                  <p class="m-n">by: BEST</p>
                </div>
              </div>
              <div class="media">
                <div class="media-left media-middle">
                  <a href="#"> <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="https://placehold.it/150x150" data-holder-rendered="true" style="width: 70px; height: 70px;"> </a> 
                </div>
                <div class="media-body p-h-xs">
                  <p class="m-n">35% Off Full HD Smart LED TV 43"</p>
                  <p class="m-n">qty: 72 of 300</p>
                  <p class="m-n">by: BEST</p>
                </div>
              </div>
              <div class="space-30"></div>
              <h4>30 points to trigger Existing Campaign</h4>
              <div class="media">
                <div class="media-left media-middle">
                  <a href="#"> <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="https://placehold.it/150x150" data-holder-rendered="true" style="width: 70px; height: 70px;"> </a> 
                </div>
                <div class="media-body p-h-xs">
                  <p class="m-n">35% Off Full HD Smart LED TV 43"</p>
                  <p class="m-n">qty: 72 of 300</p>
                  <p class="m-n">by: BEST</p>
                </div>
              </div>
              <div class="media">
                <div class="media-left media-middle">
                  <a href="#"> <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="https://placehold.it/150x150" data-holder-rendered="true" style="width: 70px; height: 70px;"> </a> 
                </div>
                <div class="media-body p-h-xs">
                  <p class="m-n">35% Off Full HD Smart LED TV 43"</p>
                  <p class="m-n">qty: 72 of 300</p>
                  <p class="m-n">by: BEST</p>
                </div>
              </div>
              <div class="media">
                <div class="media-left media-middle">
                  <a href="#"> <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="https://placehold.it/150x150" data-holder-rendered="true" style="width: 70px; height: 70px;"> </a> 
                </div>
                <div class="media-body p-h-xs">
                  <p class="m-n">35% Off Full HD Smart LED TV 43"</p>
                  <p class="m-n">qty: 72 of 300</p>
                  <p class="m-n">by: BEST</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})