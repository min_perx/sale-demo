Vue.component('campaigninfo-form', {
  template: `
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <textfield label="Campaign Name" :has-icon="true" />
              <textfield label="Campaign Description" :has-icon="true" />
            </div>
          </div>
        </div>
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Duration <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                  <datepicker label="Start Date" />
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                  <timepicker label="Time" />
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                  <datepicker label="End Date" />
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                  <timepicker label="Time" />
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                  <textfield label="Within (Optional)" />
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                  <formselect label="period"
                    :options="[
                      { value: 'minute', text: 'minute' },
                      { value: 'hour', text: 'hour' },
                      { value: 'week', text: 'week' },
                      { value: 'month', text: 'month' },
                    ]"
                  />
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-l2">
                  <div class="form-group">
                    <label>Deactivate Campaign During</label>
                    <br>
                    <div data-toggle="buttons-checkbox" class="btn-group">
                      <button class="btn btn-default" type="button" aria-pressed="true">Mon</button>
                      <button class="btn btn-default" type="button" aria-pressed="true">Tue</button>
                      <button class="btn btn-default" type="button" aria-pressed="true">Wed</button>
                      <button class="btn btn-default" type="button" aria-pressed="true">Thu</button>
                      <button class="btn btn-default" type="button" aria-pressed="true">Fri</button>
                      <button class="btn btn-default" type="button" aria-pressed="true">Sat</button>
                      <button class="btn btn-default" type="button" aria-pressed="true">Sun</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Upload Campaign Photo <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                  <img src="https://placehold.it/150x150" width="100%">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 p-h-lg p-w-m">
                  <p>HDTV-campaign-image.png(450kb)</p>
                  <button class="btn btn-primary">Replace Photo</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Tag your campaign <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <chosenselect 
                    placeholder="Select Campaigns"
                    :isMultiple="true"
                    :options="[
                      { value: 'MyRewards', text: 'My Rewards' },
                      { value: 'Homepage', text: 'Homepage' },
                      { value: 'Discover', text: 'Discover' },
                      { value: 'New', text: 'New' },
                      { value: 'MyPoint', text: 'My Point' }
                    ]" 
                  />
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <a href="#">Browse for tags</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Custom Field <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <textfield label="Textfield" />
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <a href="#">Add Custom Field</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
});