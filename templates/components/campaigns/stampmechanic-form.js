Vue.component('stampmechanic-form', {
  props: {
    id: String,
    display: { type: String, default: 'block' }
  },
  template: `
    <div :id="id" class="row" :style="{ display: [display] }">
      <div class="col-lg-8 col-sm-12 col-md-8">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Value Per Stamp <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-1 control-label">$</label>
                  <div class="col-sm-1">
                    <input type="text" class="form-control">
                  </div>
                  <label class="col-sm-1 control-label text-left">equals</label>
                  <div class="col-sm-1">
                    <input type="text" class="form-control">
                  </div>
                  <label class="col-sm-1 control-label text-left">point</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Campaign Limits(Optional) <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="form-horizontal">
                <div class="form-group">
                  <input type="checkbox" class="stamp-campaign-limit-switch col-sm-3"  />
                  <label class="col-sm-3 control-label">Disabled</label>
                </div>
                <div id="stamp-campaign-limit-switch-section" style="display:none;">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Max. Issuable in Total</label>
                    <div class="col-sm-9"><input type="text" class="form-control"></div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Max. Issuable Per Interval</label>
                    <div class="col-sm-9">
                      <div class="row">
                        <div class="col-sm-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-3">
                          <formselect
                            :options="[
                              { value: 'every', text: 'Every' },
                              { value: 'winthin', text: 'Within' }
                            ]"
                          />
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-3">
                          <formselect
                            :options="[
                              { value: 'minute', text: 'minute' },
                              { value: 'hour', text: 'hour' },
                              { value: 'day', text: 'day' },
                              { value: 'week', text: 'week' },
                              { value: 'month', text: 'month' }
                            ]"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                User Limits(Optional) <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="form-horizontal">
                <div class="form-group">
                  <input type="checkbox" class="stamp-user-limit-switch col-sm-3"  />
                  <label class="col-sm-3 control-label">Disabled</label>
                </div>
                <div id="stamp-user-limit-switch-section" style="display:none;">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Max. Issuable in Total</label>
                    <div class="col-sm-9"><input type="text" class="form-control"></div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Max. Issuable Per Interval</label>
                    <div class="col-sm-9">
                      <div class="row">
                        <div class="col-sm-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-3">
                          <formselect
                            :options="[
                              { value: 'every', text: 'Every' },
                              { value: 'winthin', text: 'Within' }
                            ]"
                          />
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-3">
                        <formselect
                            :options="[
                              { value: 'minute', text: 'minute' },
                              { value: 'hour', text: 'hour' },
                              { value: 'day', text: 'day' },
                              { value: 'week', text: 'week' },
                              { value: 'month', text: 'month' }
                            ]"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-12 col-md-4">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Code Triggers <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <p>Promo Code(Optional)</p>
              <div class="form-horizontal">
                <div class="form-group">
                  <input type="checkbox" class="stamp-promo-code-switch col-sm-3"  />
                  <label class="col-sm-3 control-label">Disabled</label>
                </div>
              </div>
              <div id="stamp-promo-code-switch-section" style="display:none;">
                <p>Parameters</p>
                <textfield label="Total scans" />
                <textfield label="Total user served" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
});