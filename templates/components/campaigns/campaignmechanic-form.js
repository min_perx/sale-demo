Vue.component('campaignmechanic-form', {
  template: `
    <div class="tabs-container">
      <ul class="nav nav-tabs">
        <li class="active">
          <a data-toggle="tab" href="#tab-1">
          <i class="fa fa-cog"></i>Mechanic
          </a>
        </li>
        <li class="">
          <a data-toggle="tab" href="#tab-2">
          <i class="fa fa-plus"></i>Add Trigger
          </a>
        </li>
      </ul>
      <div class="tab-content">
        <div id="tab-1" class="tab-pane active">
          <div class="panel-body">
            <div id="campaignTypeAccordion" data-children=".item">
              <div class="item">
                <a data-toggle="collapse" data-parent="#campaignTypeAccordion" href="#campaignTypeAccordion1" aria-expanded="true" aria-controls="campaignTypeAccordion1">
                Select Campaign Type
                </a>
                <div id="campaignTypeAccordion1" class="collapse in" role="tabpanel">
                  <p class="mb-3">
                  <mechanic-media
                    id="stampMechanic"
                    heading="Stamp Mechanic"
                    body="Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus."
                  />
                  <mechanic-media
                    id="pointMechanic"
                    heading="Point Mechanic"
                    body="Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus."
                  />
                  <mechanic-media
                    id="surveyMechanic"
                    heading="Survey Mechanic"
                    body="Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus."
                  />
                  <mechanic-media
                    id="gamificationMechanic"
                    heading="Gamification Mechanic"
                    body="Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus."
                  />
                  <mechanic-media
                    id="couponMechanic"
                    heading="Coupon Mechanic"
                    body="Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus."
                  />
                  <mechanic-media
                    id="catalogueMechanic"
                    heading="Catalogue Mechanic"
                    body="Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus."
                  />
                  </p>
                </div>
              </div>
              <div class="item">
                <a data-toggle="collapse" data-parent="#campaignTypeAccordion" href="#campaignTypeAccordion2" aria-expanded="false" aria-controls="campaignTypeAccordion2">
                Setup Mechanic
                </a>
                <div id="campaignTypeAccordion2" class="collapse in" role="tabpanel">
                  <p class="mb-3">
                    <div id="emptyForm" class="row text-center">
                      <h1>Please choose one of the Campaign Type</h1>
                    </div>
                    <stampmechanic-form 
                      id="stampMechanicForm" 
                      display="none"
                    />
                    <pointmechanic-form 
                      id="pointMechanicForm" 
                      display="none" 
                    />
                    <surveymechanic-form 
                      id="surveyMechanicForm" 
                      display="none" 
                    />
                    <gamificationmechanic-form 
                      id="gamificationMechanicForm" 
                      display="none" 
                    />
                    <couponmechanic-form 
                      id="couponMechanicForm" 
                      display="none" 
                    />
                    <cataloguemechanic-form 
                      id="catalogueMechanicForm" 
                      display="none" 
                    />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="tab-2" class="tab-pane">
          <div class="panel-body">
            <div id="exampleAccordion" data-children=".item">
              <div class="item">
                <a data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion1" aria-expanded="true" aria-controls="exampleAccordion1">
                Select Trigger Type
                </a>
                <div id="exampleAccordion1" class="collapse in" role="tabpanel">
                  <p class="mb-3">
                  <div id="voucherTrigger" class="media mechanic-media light-gray-bg" onclick="selectTrigger(this.id)">
                    <div class="media-left media-middle">
                      <img class="media-object" src="https://placehold.it/64x64" alt="image">
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading">Trigger a Voucher</h4>
                      Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus.
                      <p><a href="#">Select</a></p>
                    </div>
                  </div>
                  <!-- end of mechanic-media -->
                  <div id="campaignTrigger" class="media mechanic-media light-gray-bg" onclick="selectTrigger(this.id)">
                    <div class="media-left media-middle">
                      <img class="media-object" src="https://placehold.it/64x64" alt="image">
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading">Trigger an Existing Campaign</h4>
                      Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus.
                      <p><a href="#">Select</a></p>
                    </div>
                  </div>
                  <!-- end of mechanic-media -->
                  <div id="gamificationTrigger" class="media mechanic-media light-gray-bg" onclick="selectTrigger(this.id)">
                    <div class="media-left media-middle">
                      <img class="media-object" src="https://placehold.it/64x64" alt="image">
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading">Trigger a Gamification</h4>
                      Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus.
                      <p><a href="#">Select</a></p>
                    </div>
                  </div>
                  <!-- end of mechanic-media -->
                  </p>
                </div>
              </div>
              <div class="item">
                <a data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion2" aria-expanded="false" aria-controls="exampleAccordion2">
                Create Trigger Event
                </a>
                <div id="exampleAccordion2" class="collapse in" role="tabpanel">
                  <p class="mb-3">
                  <div id="voucherTriggerForm" style="display:none;">
                    <div class="row">
                      <div class="col-lg-8 col-sm-12 col-md-8">
                        <div class="listmanagement-borderbox">
                          <div class="ibox no-border no-margins">
                            <div class="ibox-content no-border">
                              <h4>
                                How many stamps to trigger event? <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                              </h4>
                              <div class="form-group">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                          </div>
                        </div> <!-- end of listmanagement-borderbox -->
                        <div class="listmanagement-borderbox">
                          <div class="ibox no-border no-margins">
                            <div class="ibox-content no-border">
                              <h4>
                                Select Reward <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                              </h4>
                              <div class="form-group">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#browserewards">Browse Rewards</button>
                              </div>
                            </div>
                          </div>
                        </div> <!-- end of listmanagement-borderbox -->
                      </div>
                      <div class="col-lg-4 col-sm-12 col-md-4">
                      </div>
                    </div>
                    <div class="row text-center">
                      <button type="button" class="btn btn-default">Cancel</button>
                      <button type="button" class="btn btn-primary">Save Trigger</button>
                    </div>
                  </div>
                  <div id="gamificationTriggerForm" style="display:none;">
                    <div class="row">
                      <div class="col-lg-8 col-sm-12 col-md-8">
                        <div class="listmanagement-borderbox">
                          <div class="ibox no-border no-margins">
                            <div class="ibox-content no-border">
                              <h4>
                                User Limits(Optional) <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                              </h4>
                              <div class="form-horizontal">
                                <div class="form-group">
                                  <input type="checkbox" class="gamification-2-user-limit-switch col-sm-3"  />
                                  <label class="col-sm-3 control-label">Disabled</label>
                                </div>
                                <div id="gamification-user-limit-switch-section" style="display:none;">
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Max. Issuable in Total</label>
                                    <div class="col-sm-9"><input type="text" class="form-control"></div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Max. Issuable Per Interval</label>
                                    <div class="col-sm-9">
                                      <div class="row">
                                        <div class="col-sm-3">
                                          <input type="text" class="form-control">
                                        </div>
                                        <div class="col-sm-3">
                                          <select class="select2_demo_1 form-control">
                                            <option value="1">Every</option>
                                            <option value="2">Within</option>
                                          </select>
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="text" class="form-control">
                                        </div>
                                        <div class="col-sm-3">
                                          <select class="select2_demo_2 form-control">
                                            <option value="1">minute</option>
                                            <option value="2">hour</option>
                                            <option value="3">day</option>
                                            <option value="4">week</option>
                                            <option value="5">month</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div> <!-- end of listmanagement-borderbox -->
                        <div class="listmanagement-borderbox">
                          <div class="ibox no-border no-margins">
                            <div class="ibox-content no-border">
                              <h4>
                                Probability Type <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                              </h4>
                              <div class="form-group">
                                <select data-placeholder="Select Probability Type" class="chosen-select" tabindex="2">
                                  <option value="Catalogue">Catalogue</option>
                                  <option value="Coupon">Coupon</option>
                                  <option value="Point">Point</option>
                                  <option value="Stamp">Stamp</option>
                                  <option value="Survey">Survey</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div> <!-- end of listmanagement-borderbox -->
                        <div class="listmanagement-borderbox">
                          <div class="ibox no-border no-margins">
                            <div class="ibox-content no-border">
                              <h4>
                                Gamification Method <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                              </h4>
                              <div class="form-group">
                                <select data-placeholder="Select Gamification Method" class="chosen-select" tabindex="2">
                                  <option value="ScractchCard">Scratch Card</option>
                                  <option value="Survey">Survey</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div> <!-- end of listmanagement-borderbox -->
                        <div class="listmanagement-borderbox">
                          <div class="ibox no-border no-margins">
                            <div class="ibox-content no-border">
                              <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-4">
                                  <p>Pre-play image</p>
                                  <img class="gamification-image" src="https://placehold.it/150x150">
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4">
                                  <p>Win image</p>
                                  <img class="gamification-image" src="https://placehold.it/150x150">
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4">
                                  <p>No-win image</p>
                                  <img class="gamification-image" src="https://placehold.it/150x150">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div> <!-- end of listmanagement-borderbox -->
                      </div>
                      <!-- end of col-lg-8 -->
                      <div class="col-lg-4 col-sm-12 col-md-4">
                        <div class="listmanagement-borderbox">
                          <div class="ibox no-border no-margins">
                            <div class="ibox-content no-border">
                              <h4>
                                Select Reward <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                              </h4>
                              <div class="form-group">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#browserewards">Browse Rewards</button>
                              </div>
                            </div>
                          </div>
                        </div> <!-- end of listmanagement-borderbox -->
                        <div class="listmanagement-borderbox">
                          <div class="ibox no-border no-margins">
                            <div class="ibox-content no-border">
                              <h4>
                                How many stamps to trigger event? <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                              </h4>
                              <div class="form-group">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                          </div>
                        </div> <!-- end of listmanagement-borderbox -->
                      </div>
                    </div>
                    <div class="row text-center">
                      <button type="button" class="btn btn-default">Cancel</button>
                      <button type="button" class="btn btn-primary">Save Trigger</button>
                    </div>
                  </div>
                  <div id="campaignTriggerForm" style="display:none;">
                    <div class="row">
                      <div class="col-lg-8 col-sm-12 col-md-8">
                        <div class="listmanagement-borderbox">
                          <div class="ibox no-border no-margins">
                            <div class="ibox-content no-border">
                              <h4>
                                Select Existing Campaign <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
                              </h4>
                              <div class="form-group">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#browsecampaigns">Browse Campaigns</button>
                              </div>
                            </div>
                          </div>
                        </div> <!-- end of listmanagement-borderbox -->
                        <div class="listmanagement-borderbox">
                          <div class="ibox no-border no-margins">
                            <div class="ibox-content no-border">
                              <textfield label="Campaign Type Result" :has-icon="true" />
                              <textfield label="How many points you want to give?" :has-icon="true" />
                            </div>
                          </div>
                        </div> <!-- end of listmanagement-borderbox -->
                      </div>
                      <div class="col-lg-4 col-sm-12 col-md-4">
                      </div>
                    </div>
                    <div class="row text-center">
                      <button type="button" class="btn btn-default">Cancel</button>
                      <button type="button" class="btn btn-primary">Save Trigger</button>
                    </div>
                  </div>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})