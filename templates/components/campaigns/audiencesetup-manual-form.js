Vue.component('audiencesetup-manual-form', {
  props: {
    id: String,
    display: { type: String, default: 'block' }
  },
  template: `
    <div :id="id" :style="{ display: [display] }">
      <div class="row">
        <div class="col-md-12">
          <ol class="breadcrumb">
            <li>Audience Setup</li>
            <li class="active">Manual</li>
          </ol>
        </div>
      </div>
      <div id="audiencesetup-manual-form" class="row" style="display:none;">
        <div class="col-md-6 col-md-offset-3">
          <div class="listmanagement-borderbox">
            <div class="ibox no-border no-margins">
              <div class="ibox-content no-border">
                <h4>
                  Recommended Audience Found <i class="fa fa-check-circle-o fa-2x" style="color:#38bfbd"></i>
                </h4>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="listmanagement-borderbox">
                      <div class="ibox no-border no-margins">
                        <div class="ibox-content no-border p-xs">
                          <div class="radio radio-primary">
                            <input name="audienceRadio" id="audienceRadio" type="radio">
                            <label for="audienceRadio">
                              High income, mid 20’s, male from Malaysia   <span class="text-grey">(6500 customers)</span>
                              
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="listmanagement-borderbox">
                      <div class="ibox no-border no-margins">
                        <div class="ibox-content no-border p-xs">
                          <div class="radio radio-primary">
                            <input name="audienceRadio" id="audienceRadio1" type="radio">
                            <label for="audienceRadio1">
                              High income, professionals, 40's, from Kuala Lumpur   <span class="text-grey">(5372 customers)</span>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="listmanagement-borderbox">
                      <div class="ibox no-border no-margins">
                        <div class="ibox-content no-border p-xs">
                          <div class="radio radio-primary">
                            <input name="audienceRadio" id="audienceRadio2" type="radio">
                            <label for="audienceRadio2">
                              Middle income, self-employed, retirees, from Malaysia   <span class="text-grey">(2310 customers)</span>
                              
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="listmanagement-borderbox">
                      <div class="ibox no-border no-margins">
                        <div class="ibox-content no-border p-xs">
                          <div class="radio radio-primary">
                            <input name="audienceRadio" id="audienceRadio3" type="radio">
                            <label for="audienceRadio3">
                              Low income, mid 30’s, female from Malaysia   <span class="text-grey">(387 customers)</span>
                              
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <a href="#" data-toggle="modal" data-target="#createaudience">Create New Audience</a>
                    
                  </div>
                </div>
                <div class="space-60"></div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <button type="button" class="btn btn-default" onclick="showInitial()">Back</button> 
                    <button type="button" class="btn btn-primary" onclick="goNextStep()">Apply</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
});