Vue.component('audiencesetup-auto-form', {
  props: {
    id: String,
    display: { type: String, default: 'block' }
  },
  template: `
    <div :id="id" :style="{ display: [display] }">
      <div class="row">
        <div class="col-md-12">
          <ol class="breadcrumb">
            <li>Audience Setup</li>
            <li class="active">Automatic</li>
          </ol>
        </div>
      </div>
      <div id="audiencesetup-auto-form-1" class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="listmanagement-borderbox">
            <div class="ibox no-border no-margins">
              <div class="ibox-content no-border">
                <div id="selectme">
                  <ul class="dropdownlist">
                    <li data-dropdown-text="Beauty & Health">
                      <ul>
                        <li>Luxury Beauty</li>
                        <li>Professional Skin Care</li>
                        <li>Salon & Spa</li>
                        <li>Men’s Grooming</li>
                        <li>Health, Household & Baby Care</li>
                        <li>Vitamins & Dietary Supplements</li>
                      </ul>
                    </li>
                    <li data-dropdown-text="Clothing, Shoes & Jewellery">
                      <ul>
                        <li data-dropdown-text="Women">
                          <ul>
                            <li>Clothing</li>
                            <li>Shoes</li>
                            <li>Jewelry</li>
                            <li>Watches</li>
                            <li>Handbags & Wallets</li>
                            <li>Accessories</li>
                          </ul>
                        </li>
                        <li data-dropdown-text="Men">
                          <ul>
                            <li>Clothing</li>
                            <li>Shoes</li>
                            <li>Jewelry</li>
                            <li>Watches</li>
                            <li>Accessories</li>
                          </ul>
                        </li>
                        <li data-dropdown-text="Toddlers">
                          <ul>
                            <li>Girl dresses</li>
                            <li>Boy pants</li>
                          </ul>
                        </li>
                      </ul>
                      <ul>
                        <li>Luggage</li>
                </ul>
              </li>
              <li data-dropdown-text="Electronics, Computers & Office">
                <ul>
                  <li>TV & Video</li>
                  <li>Home Audio & Theater</li>
                  <li>Camera, Photo & Video</li>
                  <li>Cell Phones & Accessories</li>
                  <li>Headphones</li>
                  <li>Video Games</li>
                  <li>Bluetooth & Wireless Speakers</li>
                  <li>Car Electronics</li>
                  <li>Musical Instruments</li>
                  <li>Wearable Technology</li>
                  <li>Electronics Showcase</li>
                  <li>Computers & Tablets</li>
                </ul>
              </li>
              <li data-dropdown-text="Food and Drink">
                <ul>
                  <li>Coffee and Tea</li>
                </ul>
              </li>
            </ul>
          </div>
                <div class="space-60"></div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <button type="button" class="btn btn-default" onclick="showInitial()">Back</button> 
                    <button type="button" class="btn btn-primary" onclick="showStep2()">Apply</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="audiencesetup-auto-form-2" class="row" style="display:none;">
        <div class="col-md-6 col-md-offset-3">
          <div class="listmanagement-borderbox">
            <div class="ibox no-border no-margins">
              <div class="ibox-content no-border">
                <h4>
                  Recommended Audience Found <i class="fa fa-check-circle-o fa-2x" style="color:#38bfbd"></i>
                </h4>
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="ibox padding-0 borderbox margin-20">
                      <div class="ibox-content padding-0 no-border">
                        <div class="row">
                          <div class="col-lg-12 col-container">
                            
                            <div class="col-lg-4 nobspadding bg-primary col">
                              <i class="fa fa-male"></i>
                            </div>
                            
                            <div class="col-lg-8 col2">
                              <h2>938</h2>
                              Total Male
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="ibox padding-0 borderbox margin-20">
                      <div class="ibox-content padding-0 no-border">
                        <div class="row">
                          <div class="col-lg-12 col-container">
                            
                            <div class="col-lg-4 nobspadding bg-primary col">
                              <i class="fa fa-female"></i>
                            </div>
                            
                            <div class="col-lg-8 col2">
                              <h2>673</h2>
                              Total Female
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="ibox padding-0 borderbox margin-20">
                      <div class="ibox-content padding-0 no-border">
                        <div class="row">
                          <div class="col-lg-12 col-container">
                            
                            <div class="col-lg-4 nobspadding bg-primary col">
                              <i class="fa fa-location-arrow"></i>
                            </div>
                            
                            <div class="col-lg-8 col2">
                              <h2>Kuala Lumpur</h2>
                              Region
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="ibox padding-0 borderbox margin-20">
                      <div class="ibox-content padding-0 no-border">
                        <div class="row">
                          <div class="col-lg-12 col-container">
                            
                            <div class="col-lg-4 nobspadding bg-primary col">
                              <i class="fa fa-usd"></i>
                            </div>
                            
                            <div class="col-lg-8 col2">
                              <h2>Medium to High</h2>
                              Income Bracket
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="ibox padding-0 borderbox margin-20">
                      <div class="ibox-content padding-0 no-border">
                        <div class="row">
                          <div class="col-lg-12 col-container">
                            
                            <div class="col-lg-4 nobspadding bg-primary col">
                              <i class="fa fa-line-chart"></i>
                            </div>
                            
                            <div class="col-lg-8 col2">
                              <h2>Frequent</h2>
                              Patronage
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="ibox padding-0 borderbox margin-20">
                      <div class="ibox-content padding-0 no-border">
                        <div class="row">
                          <div class="col-lg-12 col-container">
                            
                            <div class="col-lg-4 nobspadding bg-primary col">
                              <i class="fa fa-shopping-cart"></i>
                            </div>
                            
                            <div class="col-lg-8 col2">
                              <h2>2 - 5 months ago</h2>
                              Last Purchase
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="space-60"></div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <button type="button" class="btn btn-default" onclick="showStep1()">Back</button> 
                    <button type="button" class="btn btn-primary" onclick="goNextStep()">Save and Next</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
});