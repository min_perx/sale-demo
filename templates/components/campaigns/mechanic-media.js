Vue.component('mechanic-media', {
  props: {
    id: String,
    heading: String,
    body: String
  },
  template: `
    <div :id="id" class="media mechanic-media light-gray-bg" onclick="selectMechanic(this.id)">
      <div class="media-left media-middle">
        <img class="media-object" src="https://placehold.it/64x64" alt="iGage">
      </div>
      <div class="media-body">
        <h4 class="media-heading">{{ heading }}</h4>
        {{ body }}
        <p><a href="#">Select</a></p>
      </div>
    </div>
  `
});