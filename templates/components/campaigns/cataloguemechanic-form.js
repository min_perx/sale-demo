Vue.component('cataloguemechanic-form', {
  props: {
    id: String,
    display: { type: String, default: 'block' }
  },
  template: `
    <div :id="id" class="row" :style="{ display: [display] }">
      <div class="col-lg-8 col-sm-12 col-md-8">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Catalogue Trigger <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="form-group">
                <label>Select Existing Loyalty Programme</label>
                <select data-placeholder="The 1 Card" class="chosen-select" tabindex="2">
                  <option value="Catalogue">Catalogue</option>
                  <option value="Coupon">Coupon</option>
                  <option selected value="Gamification">Gamification</option>
                  <option value="Point">Point</option>
                  <option value="Stamp">Stamp</option>
                  <option value="Survey">Survey</option>
                </select>
                <div class="space-20"></div>
                <select data-placeholder="Select Membership Level" class="chosen-select" tabindex="2">
                  <option value="Catalogue">Catalogue</option>
                  <option value="Coupon">Coupon</option>
                  <option selected value="Gamification">Gamification</option>
                  <option value="Point">Point</option>
                  <option value="Stamp">Stamp</option>
                  <option value="Survey">Survey</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Campaign Limits(Optional) <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="form-horizontal">
                <div class="form-group">
                  <input type="checkbox" class="catalogue-campaign-limit-switch col-sm-3"  />
                  <label class="col-sm-3 control-label">Disabled</label>
                </div>
                <div id="catalogue-campaign-limit-switch-section" style="display:none;">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Max. Issuable in Total</label>
                    <div class="col-sm-9"><input type="text" class="form-control"></div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Max. Issuable Per Interval</label>
                    <div class="col-sm-9">
                      <div class="row">
                        <div class="col-sm-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-3">
                          <select class="select2_demo_1 form-control">
                            <option value="1">Every</option>
                            <option value="2">Within</option>
                          </select>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-3">
                          <select class="select2_demo_2 form-control">
                            <option value="1">minute</option>
                            <option value="2">hour</option>
                            <option value="3">day</option>
                            <option value="4">week</option>
                            <option value="5">month</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                User Limits(Optional) <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <div class="form-horizontal">
                <div class="form-group">
                  <input type="checkbox" class="catalogue-user-limit-switch col-sm-3"  />
                  <label class="col-sm-3 control-label">Disabled</label>
                </div>
                <div id="catalogue-user-limit-switch-section" style="display:none;">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Max. Issuable in Total</label>
                    <div class="col-sm-9"><input type="text" class="form-control"></div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Max. Issuable Per Interval</label>
                    <div class="col-sm-9">
                      <div class="row">
                        <div class="col-sm-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-3">
                          <select class="select2_demo_1 form-control">
                            <option value="1">Every</option>
                            <option value="2">Within</option>
                          </select>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-3">
                          <select class="select2_demo_2 form-control">
                            <option value="1">minute</option>
                            <option value="2">hour</option>
                            <option value="3">day</option>
                            <option value="4">week</option>
                            <option value="5">month</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-12 col-md-4">
        <div class="listmanagement-borderbox">
          <div class="ibox no-border no-margins">
            <div class="ibox-content no-border">
              <h4>
                Code Triggers <i class="fa fa-info-circle" style="color:#d1d1d1" data-toggle="tooltip" data-placement="right" title="Info"></i>
              </h4>
              <p>Promo Code(Optional)</p>
              <div class="form-horizontal">
                <div class="form-group">
                  <input type="checkbox" class="catalogue-promo-code-switch col-sm-3"  />
                  <label class="col-sm-3 control-label">Disabled</label>
                </div>
              </div>
              <p>Parameters</p>
              <div class="form-group">
                <label>Total scans</label>
                <input type="text" class="form-control">
              </div>
              <div class="form-group">
                <label>Total user served</label>
                <input type="text" class="form-control">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
});